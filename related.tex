\section{Background}
\label{sec:bg}

\paragraph{Probabilistic logic and Markov Logic Networks.} Probabilistic
logic~\citep{Nilsson:1986te} has a probability distribution over
worlds. In the finite case, which is the relevant case here, the
probability of a sentence of the logic formula $\phi$ is the summed
probability of worlds in which $\phi$ is true: 
\[p(\phi) = \sum_{w: w
  \models \phi} p(w)\] 
Markov Logic Networks~\citep{richardson:mlj06,domingos:book09} (MLNs) employ weighted formulas in first-order logic to
compactly encode complex undirected probabilistic graphical models (i.e.,
Markov networks). Weighting the rules softens them compared to hard logical
constraints, thereby allowing situations in which not all clauses are
satisfied.  
In MLNs, the weighted formulae determine the probability of a world, 
where a world's probability increases exponentially with the total weight of
grounded logical clauses satisfied.  The probability of a given world $x$ is denoted by:
\[
P(X=x) = \dfrac{1}{Z} \exp \left( \sum_{i}{w_i n_i \left( x \right)}
\right)
\]
where $Z$ is the partition function, $i$ ranges over all formulas
$F_i$,  
$w_i$ is the weight of $F_i$ and $n_i(x)$ is the number of true groundings 
of $F_i$ in the world $x$. Marginal inference in MLNs calculates the probability 
$P(Q|E,F_w)$, where $Q$ is a query, $E$ is the evidence set, and $F_w$ is the set of 
weighted formulas. MLN inference tasks resulting from logic-based
representations of the semantics of natural language sentences tend to have many
predicates and long formulas, resulting in an extremely large number of ground
clauses and hence extremely large Markov networks. 

\paragraph{Textual entailment.} 
The 
{\it recognizing textual entailment} (RTE) task~\citep{dagan:hlt13}
requires systems to determine whether one natural language text, the \textit{Text $T$}, \emph{Entails}, 
\textit{Contradicts}, or is \emph{Neutral} with respect to another, the \textit{Hypothesis $H$}. 
Here are examples from the SICK dataset~\citep{marelli:lrec14}:

\begin{tabular}{ll}
  \textbf{Entailment:} & \begin{tabular}[t]{ll}
T: & A man and a woman are walking together through the woods.\\
H: & A man and a woman are walking through a wooded area.\\
                    \end{tabular}
\\[5ex]
\textbf{Contradiction:} & \begin{tabular}[t]{ll}
T: & A man is jumping into an empty pool.\\
H: & A man is jumping into a full pool.\\
                      \end{tabular}
\\[5ex]
\textbf{Neutral:} & \begin{tabular}[t]{ll}
T: & A young girl is dancing.\\
H: & A young girl is standing on one leg.\\
                \end{tabular}
\end{tabular}

Textual entailment will be the main evaluation task for part (1) of
our project, probabilistic semantics. 

\paragraph{Probabilistic logic for natural language semantics.}
Logic-based natural language semantics has a long tradition~\citep{Montague:73,dowty:book81,kamp:book93}. Recent
wide-coverage tools that generate logic-based sentence representations
include \citet{CopestakeFlickinger},
\citet{bos:step08}, and \citet{Lewis:tf}. Bos et al.\ use logic-based
sentence representations for
RTE~\citep{BosMarkert:06,bos:lilt13,bjerva-EtAl:2014:SemEval}. As they
use standard first-order theorem provers, all lexical information has
to be made binary, which leads to problems particularly with
automatically generated paraphrase collections because of the noise
they contain~\citep{bjerva-EtAl:2014:SemEval}. \citet{Lewis:tf}
cluster distributional information into induced word senses. While
they can associate sense combinations with probabilities, these
probabilities do not enter into sentence-level inference, which is
again standard first-order inference. Our approach differs from both
of these in that we use probabilistic  inference, which takes weights
into account. 

Our own prior work on probabilistic logic for natural language
semantics~\citep{garrette:iwcs11,Garrette:bkchapter13,beltagy:starsem13,beltagy:acl14,beltagy:sp14,beltagy:starai14}
integrates logic and distributional information to perform textual
entailment. The first component of our framework is Bos' system Boxer
\citep{bos:step08}, which parses each 
sentence using the C\&C CCG parser \citep{clark:acl04} and then maps its output
into a logical form in which the predicates are uninterpreted words.
For example, ``A man is driving a car'' is mapped to
\[\exists x, y, z. \: man(x) \wedge agent(y, x) \wedge drive(y) \wedge
patient(y, z) \wedge car(z)\]
Boxer outputs discourse representation structures (DRSs,
\citep{kamp:book93}), which are then mapped to first-order logic via a
standard transformation. 
Next, distributional information is encoded in the form of weighted inference
rules connecting words and phrases in the text $T$ and hypothesis $H$
of an RTE problem.  For example, for $T$: ``A
man is driving a car'', and $H$: ``A guy is driving a vehicle'', the system
generates rules including 
\[\begin{array}{ll}
\forall x. \: man(x) \Rightarrow guy(x)  &| \: w_1\\
\forall
x. \: car(x) \Rightarrow vehicle(x) & | \: w_2\\
  \end{array}
  \]
where $w_1$ and $w_2$ are
weights indicating the similarity of the antecedent and
consequent. (It also generates further rules, for example one linking ``man'' to ``vehicle'',
as the mapping is not known a priori.)
Specifically, for all pairs $(a, b)$, where $a$ and $b$ are words or phrases of
$T$ and $H$ respectively, it generates an inference rule $a \rightarrow b \mid
w$, where the rule  weight is $w = \log odds\big(sim(\vec{a}, \vec{b})\big)$,
and $sim$ is the cosine similarity of the vectors $\vec{a}$ and
$\vec{b}$. 
In the existing prototype system, the word vectors are out-of-context
vectors, and vector representations of phrases are simply computed using vector addition across the component
predicates \cite{mitchell:jcs10}. Additional lexical inference rules
come from paraphrase databases
~\citep{Berant:2011te,Ganitkevitch:2013tf} and
WordNet~\citep{Fellbaum:98}. 

For an RTE problem with text $T$ and hypothesis $H$, we
compute a set $KB$ of lexical inference rules from distributional
evidence, paraphrase datbases, and WordNet. We then compute two
probabilities, $P(H|T, KB)$ and $P(\neg
T|H, KB)$
in order to test for both entailment and contradiction. In the first
case, $H$ becomes the query $Q$, $T$ contributes both evidence and
rules, and $KB$ contains further rules. In the second case $\neg T$
becomes the query. 
The inference is performed using Alchemy~\citep{kok:tr05}, the most widely used MLN implementation. 
A
supervised classifier finally maps this pair of probabilities to 
Entailment, Neutral, or Contradiction.  

Our recent work on the framework includes an approach that
drastically reduces the size of the Markov network by testing which
ground literals are reachable from the text
$T$~\citep{beltagy:starai14}. Non-reachable ground literals are
assumed false, such that the system overall makes a variant of the
closed-world assumption. We have also made modifications to the
computed logical form to account for the fact that Markov Logic
Networks uses a finite domain and makes the domain closure
assumption, an assumption of one-to-one correspondence of objects in
the domain and constants in the given set of
formulas. This led to problems, among other things with processing
universal quantifiers, which were corrected through additional
constants that were added to the evidence
set~\citep{beltagy:semeval14}. 
In the project proposed here, we will build on our existing prototype
system, improving both its accuracy and its coverage in a
principled manner.%  -- the former for example by addressing problems
% introduced by the closed-world assumption, the latter by adding
% coverage of determiners like ``most''. 


% Well-known problem of MLNs: high memory use for computing all
% groundings. Approach specific to the natural language semantics
% application: Modified closed world assumption, detect un-inferrable
% literals and prevent them from being added to the
% graph~\citep{beltagy:starai14}. Also experimented with Probabilistic
% Soft Logic (PSL, \citep{broecheler:uai10}) with good results on the STS
% task. Not pursued further here, as PSL shares the problems of fuzzy
% logic that come from a truth-functional
% approach~\citep{vanDeemter:truthfunctional}.

% Recent improvements of meaning representation: Practical probabilistic
% logic systems, including MLNs, typically make the \emph{Domain Closure
%   Assumption}: one-to-one correspondence between entities in the universe
% and constants in the logic. Need to introduce sufficient constants to
% ensure a nonempty network, which can be done based on presupposition
% that the domain of most quantifiers is
% nonempty~\citep{Strawson:50,Geurts:07}. Another problem with the
% domain closure assumption is that from \textit{(a) Tweety is a bird that
%   flies} it can be inferred that \textit{(b) all birds fly}, as the
% universe considered for (a) will consist of a single entity,
% Tweety. So to test whether (b) is entailed by a given collection $F$
% of sentences, we introduce a new constant, say Charlie, and
% state that it is a bird. If $F$ entails that all birds fly, but not in
% the case of (a), we will be able to infer that Charlie
% flies. \textbf{too lengthy. this needs to be shortened or removed}


\paragraph{Distributional models of word meaning in context.} Word
meaning is notoriously difficult to describe. Senses are often
difficult to define~\citep{Kilgarriff:97,Hanks:00}, and the boundary
between distinct senses and related uses (ambiguity and vagueness) seems to be
fuzzy~\citep{Tuggy93,Cruse:95}. As a case in point, \citet{Tuggy93}
uses the verb ``paint'', where standard linguistic tests on ambiguity
versus vagueness yield ambivalent results: The sentence ``I have been
painting, and so has Jane'' is normal if I have been painting in
watercolor and Jane in oil. It is clearly odd if Jane has been painting a
portrait while I have been painting stripes on the road. But if Jane
has been painting a mural, while I have painted a wall in a uniform color,
Tuggy says that the sentence seems ``almost, but still not entirely,
normal''. So it appears that the different uses of ``paint'' vary in
their pairwise distance, with the most distant pairs seeming clearly
distinct and the closest pairs seeming indistinguishable, and
medium-distant pairs forming borderline cases where it is hard to
decide whether they should be described as having the same
sense. Based on this evidence, this project describes lexical information in a way
that does not rely on the existence of fixed, distinct senses. 

How can meaning in context be described other than through a single best
sense? One option, which was first co-introduced by the
PI~\citep{ErkPado:08} and which has been explored by a number of
groups
since~\citep{dinu-lapata:2010:EMNLP,ErkPado:10,ReisingerMooney:10,ThaterFuerstenauPinkal:10,thater11:word_meanin_context,vandecruys-poibeau-korhonen:2011:EMNLP,Dinu2012},
is to compute distributional representations for individual word
instances. Word instances thus form points in a semantic space at
different distances, matching what we saw in the case of ``paint''
above. % Some of these distributional approaches induce latent
% senses~\citep{dinu-lapata:2010:EMNLP,ReisingerMooney:10}, others
% re-weight dimensions of an out-of-context distributional
% representation~\citep{ErkPado:08,ThaterFuerstenauPinkal:10,thater11:word_meanin_context,vandecruys-poibeau-korhonen:2011:EMNLP}. 
Surprisingly,
most
approaches, and in particular those that constitute the state-of-the-art, rely solely on information from syntactic neighbors to
compute an instance representation. An existing approach that tried to
combine syntax with wider context was restricted to small amounts of
data~\citep{vandecruys-poibeau-korhonen:2011:EMNLP}. In this project,
we develop new distributional approaches to describe word meaning in
context in a graded fashion. One of our aims is to be able to combine
syntactic information with information from wider context in a robust fashion.

\begin{table}[tb]
  \centering
  \begin{tabular}{|p{25em}|p{12.8em}|}\hline
    Sentence & Substitutes\\\hline\hline
When I think of Julia Child I think of the television episode where
    she's \underline{showing} you how to make a turkey dinner and the
    turkey fell on the floor.
& 
exhibit, demonstrate, present, display, tell, \textit{explain}, \textit{instruct}, \textit{teach}\\\hline


  \end{tabular}
  \caption{Lexical substitution: Example annotation from the dataset
    of \citet{coinco}. Underlined: target word ``show''. In italics:
    Substitutes that are not synonyms, hypernyms or hyponyms of
    ``show'' (WordNet).}
  \label{tab:lexsub_example}
\end{table}

\paragraph{Lexical Substitution.} The main way in which existing
distributional models of word meaning in context have been evaluated is through the lexical substitution
task~\citep{MccarthyNavigli:09}, which in this context is typically
phrased as a ranking task: Given a target word $t$ in a sentence
context $c$, and given a set of substitute candidates $s_1, \ldots,
s_n$, rank the $s_i$ by their appropriateness as substitutes for $t$
in $c$. Lexical substitution
annotation consists of lexical substitutes (one-word paraphrases) for
a word in context, collected from multiple annotators to yield a
fine-grained characterization of the instance. Three datasets are now
available~\citep{MccarthyNavigli:09,Biemann:2013,coinco}, the most
recent of which is an all-words lexical substitution dataset that we
collected recently. 
Table~\ref{tab:lexsub_example} shows an example of
a word instance annotated with lexical substitutes, illustrating
another reason to move away from dictionary approaches to 
word meaning: Some of the human-generated substitutes are not
synonyms, hyponyms or hypernyms of the target (here shown in
italics), in fact, this is the case for over 60\% of the substitutes
in the \citet{coinco} dataset. A distributional approach can in principle infer (by
proximity in space) that this instance of ``show'' is a ``teaching''
event, while this is difficult with dictionary senses. For part (2) of
the project, where we focus on word meaning in context, lexical
substitution and RTE will be the main tasks that we consider, with
lexical substitution allowing for a fine-grained focus on the
phenomenon and RTE being more task-based. 


 
\paragraph{Word2Vec and the Skip-Gram Negative Sampling Algorithm.}
The Skip-Gram Negative Sampling Algorithm (SGNS)
\cite{Mikolov:2013:ICLR:efficient} is a recent algorithm for developing
low-dimensional distributional vectors. The model is based on a the classic
logistic regression model, but the procedure learns vector representations for
both words and contexts simultaneously, with the goal of predicting whether a
word $w$ appears with a context $c$,

\begin{equation}
  P(w|c) = \sigma(\vec{w}\cdot\vec{c}),
  \label{eqn:word2vec}
\end{equation}
where $\sigma(x)$ is the logistic sigmoid function, 
\[
\sigma(x) = \frac{1}{1 + \exp(x)}
\]
The procedure updates the representations $\vec{w}$ and $\vec{c}$
together by using positive examples extracted from the corpus, and sampling negative
examples from the unigram distributions, maximizing the log-likelihood over an entire
corpus $D$,
\begin{equation}
  J = \sum_{(w, c) \in D}\log\sigma(\vec{w}\cdot\vec{c}) + k\cdot\mathrm{E}_{n \sim P_D}\left[\log\sigma(-\vec{w}\cdot\vec{n})\right],
  \label{eqn:w2vglobal}
\end{equation}
where $n \sim P_D$ indicates sampling a context from the unigram distribution, and
$k$ is a hyperparameter indicating the number of negative samples to draw.
The algorithm is often known colloquially as
Word2Vec, the name of its canonical
implementation. %\footnote{\url{https://code.google.com/p/word2vec/}} 
% KE: OSP doesn't let us have URLs in the project description.
The algorithm was first made popular when it was discovered it could perform
analogy prediction via simple arithmetic: namely, $\vecn{king} - \vecn{man} +
\vecn{woman} \approx \vecn{queen}$
\citep{mikolov:nips13}.  In addition to this intruiging property, vectors
derived using SGNS perform state-of-the-art or nearly state-of-the-art on
standard benchmarks including
semantic relatedness, synonym detection, and concept categorization
\citep{Mikolov:2013:ICLR:efficient,Mikolov:2013tp,mikolov:nips13,baroni-dinu-kruszewski:2014:P14-1}.

Since the introduction of the SGNS algorithm, several modifications have been
proposed. Of particular
interest, \citet{LevyGoldberg:2014:ACL} showed how the model could be expanded
to arbitrary definitions of contexts, such as contexts generated by extracting
relation tuples from dependency parses. Distributional spaces generated from
dependency relations are well studied, and are particularly suited for expressing
the selectional preferences of words
\citep{PadoLapata:07,ErkPado:08,ErkPado:10,LevyGoldberg:2014:ACL}. We
will build on the SGNS algorithm, and in particular its extension to
handling dependency relations, as a basis for the models for word
meaning in context that we construct. 



% Analyzing the substitutes in this dataset, we
% found that over 60\% of the substitutes were not synonyms, hyponyms, or
% hypernyms of the target according to WordNet. A few of these
% substitutes point to missing entries in
% WordNet, but most are cases of contextual
% enrichment~\citep{Cruse:95}. As an example, consider the sentence ``When I think of Julia
% Child I think of the television episode where she's \textit{showing} you how to
% make a turkey dinner and the turkey fell on the floor.'' Some
% substitutes provided for ``show'' in this context are WordNet-related,
% such as ``demonstrate''. The substitutes that are not WordNet-related
% tend to be more situation-specific, in this case ``explain'', ``instruct'', and
% ``teach''. This is a phenomenon that is not explicitly addressed
% by any 




% \paragraph{Distributional evidence in human sentence understanding.} Question of to what extent people use distributional information in language understanding often discussed~\citep{LandauerDumais:97,McDonaldRamscar:01,Murphy:02,Louwerse:07,Lenci:08,Barsalou:2008,AndrewsViglioccoVinson:09,Louwerse:2010}. More recently also the question of integration with a formal account of semantics~\citep{vanEijckLappin,Copestake:2012wm,Erk:IWCS13,Larsson:13}. We recently started on account that assumes: 
% representation of lexical items both at a truth-conditional level, including explicit knowledge about relations between words, and at distributional level, encoding degrees of similarity. Explicit truth-conditional representation helps interpret distributional representations, and distributional representations reduce speaker's uncertainty about meaning of lexical items about whose meaning they are uncertain. Framed in probabilistic logic setting, with distributional evidence updating the probability distribution over worlds that represents the speaker's information status~\citep{ErkDraft}. Only starting point, in particular need to determine character of distributional inference, and role of distributional evidence in characterizing word meaning in context. 

\paragraph{Distributional evidence in human sentence understanding.} Distributional information has proved very useful in NLP. But does
it have any role in human language understanding? This question is
being widely discussed in
psychology~\citep{LandauerDumais:97,McDonaldRamscar:01,Louwerse:07,AndrewsViglioccoVinson:09,Johns:2012gn}. There
is also a beginning debate on the issue in formal
semantics~\citep{Copestake:2012wm,Larsson:13,vanEijckLappin,Zeevat:2014wt},
but the issue is: If we accept that
lexical knowledge has to do with truth conditions, then what can the
role be for distributional information, which just links words to
other words? In a paper~\citep{ErkDraft} that is currently under revision for the
Journal of Semantics and Pragmatics, we suggest that
speakers are capable of observing correlations between observations in
the real world on the one hand, and distributional patterns on the
other hand, and that they can use these correlations to make
\emph{probabilistic} inferences about the meaning of unknown words: ``I don't
know what an alligator is, but it must be something like a
crocodile''. What is this probabilistic inference about? We argue that distributional inference is
\emph{property inference}, that is, that distributionally similar words have
similar semantic features (at least for distributional models based on narrow
context windows or on syntactic context): If ``alligator'' is an unknown word but
distributionally similar to ``crocodile'', we infer that alligators
share many features with crocodiles. We frame this inference in
terms of probabilistic logic, representing the information state of a
speaker as a probability distribution over worlds. 
Given a piece of distributional evidence, for example that
``alligator'' is highly similar to ``crocodile'', this probability
distribution is updated (defined as Bayesian update), increasing the
probability of worlds in which the denotations of ``alligator'' and
``crocodile'' share many properties, and decreasing the probability of
worlds where this is not the case. 
The approach thus fits in with  a growing number of papers
that look at probabilistic approaches in a formal semantics
framework~\citep{VanBenthem:2009te,NotExactly,Copestake:2012wm,Larsson:13,Zeevat:2013wc,Cooper:2014,vanEijckLappin,GoodmanLassiter,Zeevat:2014wt}. 
