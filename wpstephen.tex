
Distributional information has been successfully integrated into logical
inference systems before, both by
us~\citep{garrette:iwcs11,Garrette:bkchapter13,beltagy:starsem13} and
by \citet{,Lewis:tf}, 
as a critical component for detecting lexical entailment (that is,
the recognition of paraphrases at the lexical
level~\citep{geffetdagan2005ACL}). 
But our existing prototype system uses
a single, context-independent representation per word, ignoring polysemy, and
Lewis and Steedman first perform disambiguation between distributionally
induced senses and then do standard first-order
inference with the inferred sense labels. In contrast to Lewis and
Steedman, we want to incorporate the distributional weights into
inference at the sentence level, and in contrast to our previous
prototype system, we want to work with distributional
information that takes sentence context into account. The aim of this
work package is to develop a novel distributional model of word
meaning in context.

Evaluation will both be in a targeted manner through  lexical
substitution and contextual word
similarities~\citep{HuangEtAl:2012:ACL}, and task-based through RTE. 
%  For lexical
% substitution, we will use all three existing
% datasets. \citet{MccarthyNavigli:09}, the most widely used dataset,
% has selected verbs, nouns, adjectives, and adverbs, with 10 sentences
% per lemma. \citet{Biemann:2013} focuses on high-frequency
% nouns. \citet{coinco} has all-words lexicl substitution annotation of
% both news and fiction, with substitutes annotated for their
% WordNet-relation to the target (synonym, hyponym, hypernym, other),
% allowing for fine-grained evaluation. The contextual word similarities
% dataset~\citep{HuangEtAl:2012:ACL} has word similarity ratings like
% WordSim353 does, but uses word
% pairs in given sentential contexts. 
While distributional models
of word meaning in context have up to now mainly been evaluated on
focused tasks like lexical substitution, it will be important to see
how appropriate they are for doing lexical entailment in a textual
entailment setting, and also to see to what extent probabilistic
inference can make better use of context-specific lexical weights than
out-of-context weights. The SICK RTE
dataset~\citep{marelli:lrec14} takes an in-between position between
focused and task-based evaluation as it contains
naturally occurring sentences manually manipulated to test, among
other things, lexical entailment. 


% keep the weights coming from the distributional model 
% predicting the degree of lexical entailment. However, these
% approaches use only one representation per word, and ignore a great deal of work
% on handling polysemy and contextualized word meanings in distributional spaces
% \citep{dinu-lapata:2010:EMNLP,ErkPado:10,ThaterFuerstenauPinkal:10,thater11:word_meanin_context,vandecruys-poibeau-korhonen:2011:EMNLP,Dinu2012,HashimotoEtAl:2014:ACL}.
% Systems which attempt to make logical inference based over ambiguous words will inevitably
% stumble on sentences like ``A {\em bright} boy is reading in the dark,'' likely leading to
% incorrect conclusions about the luminosity of the boy.



\paragraph{WP2.1 Word2Vec Lexical Substitution.}
The Word2Vec algorithm learns vector representations for
words and contexts simultaneously with the aim of predicting whether a
word $w$ appears with a context $c$, as described in
Equation~\eref{eqn:word2vec}. Given that Word2Vec models have shown improvement over traditional vector spaces
in numerous tasks \citep{Mikolov:2013:ICLR:efficient,Mikolov:2013tp,baroni-dinu-kruszewski:2014:P14-1},
we hope this model will provide similar improvements on the Lexical Substitution task.
Its syntactic-context variation described by
\citet{LevyGoldberg:2014:ACL} is naturally suited for the task of predicting
lexical substitution in context, as in that case the model 
explicitly defines a conditional probability for a word appearing in a given
syntactic context. Suppose we have a target word $t$ in a sentence
represented as a set $C$ of syntactic neighbors of $t$. (For example, in
``The girl \underline{chased} the cat'' with target ``chase'', $C$ would
contain subj-girl and obj-cat.) Then, with representations fixed after training from a
corpus, the log-likelihood of a particular substitute candidate $s$ of
$t$ appearing in $C$ is given by
\begin{equation}
  \log P(s|c) = \sum_{c \in C}\log\sigma(\vec{s}\cdot\vec{c}).
  \label{eqn:substitute}
\end{equation}
This explicitly models selectional preferences of each candidate substitute $s$,
allowing it to exploit much of the same information as state-of-the-art systems
\citep{thater11:word_meanin_context,Dinu2012}, while also providing the well-formed
probability distribution available in other systems
\citep{vandecruys:2008:PAPERS,dinu-lapata:2010:EMNLP,vandecruys-poibeau-korhonen:2011:EMNLP}.
While most distributional approaches to word meaning in context first
explicitly compute a context-specific representation $\vec{t'}$ for a
target word $t$, the formulation of Equation~\eref{eqn:substitute}
disregards the target (except as the source of the list of substitute
candidates) and directly measures the fit of each
substitute. 

The Word2Vec approach also lends itself to another
natural formulation. We can compute the maximally compatible word
vector $\vec{\hat{x}}$ for the given context, 
\begin{equation}
  \vec{\hat{x}} = \argmax_{\vec{x}} \sum_{c \in C}\log\sigma(\vec{x}\cdot\vec{c}).
  \label{eqn:argmaxt}
\end{equation}
and then determine a context-specific representation $\vec{t'}$ for
the target. This can be 
either $\vec{\hat{x}}$ or a combination of $\vec{\hat{x}}$ and
$\vec{t}$. After that, substitutes are ranked by similarity to
$\vec{t'}$. We will experiment with 
% The aim of this work package is to develop a distributional model for
% word meaning in context that shows good performance and is extensible
% in further experiments (WP2.2 below). In the search for this model, we
% will experiment with 
both models in
Equations (\ref{eqn:substitute}) and (\ref{eqn:argmaxt}). 
% These
% experiments will include a study of parameters such as the best way to
% determine $\vec{t'}$ in the second model: by setting $\vec{t'} =
% \vec{\hat{x}}$ or by setting $\vec{t'} = \vec{t} \otimes
% \vec{\hat{x}}$ for one out of a number of possible combinators
% $\otimes$. 

% \paragraph{WP2.2 Distributional Smoothing.} 
% One of the key components of the state-of-the-art
% approaches to the Lexical Substitution task appear to be that they
% compute the context-specific representation of a target $t$ based not (or not only) on the observed syntactic neighbors but on distributional
% representations of those
% neighbors~\citep{ThaterFuerstenauPinkal:10,Dinu2012}, a method that
% we call {\em distributional smoothing}.
% For example, \citet{ThaterFuerstenauPinkal:10} models the meaning of
% ``take'' in ``\emph{take} [the] bus'' by modifying the out-of-context
% distributional representation of ``take'' through the distributional
% representation of ``bus'', and \citet{thater11:word_meanin_context}
% will take into account not only the neighbor ``bus as object'', but
% also distributionally similar words, for example ``car as object'' and
% ``train as object''. 
% % averaging the representation of {\em boy} with its nearest
% % neighbors (e.g., {\em girl}, {\em child}, {\em man}). 
% % KE: I really don't see what you mean by "averaging" here. 
% While this approach has been
% successful, it remains unclear how {\em much} or {\em when} smoothing
% is beneficial. % , and
% % how this same idea of distributional smoothing can be implemented in continuous
% % word representations like Word2Vec \textbf{really difficult, or only
% %   when you concentrate on Thater 2011?}. 
% Smoothing clearly should be helpful
% in the ``take bus'' example given above, but is probably not as helpful in more
% idiomatic examples like ``take long:'' Generalizing from ``long'' to
% ``short'' and ``tall'' is likely to lead to worse results than using
% the unsmoothed form. 

% We propose experiments directly measuring
% how different degrees of smoothing affect performance on the Lexical
% Substitution task. We also propose exploring and evaluating different ways in
% which this smoothing can be implemented in the Word2Vec model for the Lexical
% Substitution task. For example, one very simple approach would be to extend the
% list of contexts given $C$ to include additional similar contexts,
% \begin{equation*}
%   \hat C = C \cup \{v~|~cosine(c, v) > \delta \text{ for } c \in C, v \in V\}.
% \end{equation*}
% Another possible approach more similar to that of \citet{thater11:word_meanin_context}
% would be modify each of the given contexts $\vec{c}$ by averaging (or weighted averaging) it
% with its $N$ nearest neighbors,
% \begin{equation*}
%   \hat c = \frac{1}{N+1}\left(\vec{c} + \sum_{i=1}^N\text{nearest-neighbors}(c, i)\right).
% \end{equation*}
% Each of these proposed models has a parameter to explicitly control the degree
% of smoothing ($\delta$ and $N$), allowing us to explore how much smoothing is
% ideal. Both of these models, and potentially others, will be evaluated on all
% the proposed tasks.

\paragraph{WP2.2 Integrating Wider Contexts.}
Most current distributional approaches to characterizing word meaning in context only exploit local syntactic
context, or an extremely narrow bag-of-words window
\cite{thater11:word_meanin_context,Dinu2012,HashimotoEtAl:2014:ACL}. This information
is obviously necessary, as substitutes must syntactically agree with
the target context. But
these approaches ignore a huge amount of information available in the
wider sentence, and especially given that traditional word sense
disambiguation models typically use (and need) both syntactic features
and bag-of-words context features describing the wider
context~\citep{LNC3:LNC3131}, it is to be expected that using
information about wider context would be beneficial to distributional
models of word meaning in context as well. Indeed,
\citet{vandecruys-poibeau-korhonen:2011:EMNLP} show there are performance improvements
to the lexical substitution task by employing both the syntactic context {\em and} a bag-of-words
of the rest of the sentence. But their model makes it difficult
for them to make use of larger amounts of data.
In this work package, we study ways of combining syntactic information
with additional information about wider context in context-aware
distributional models, building on WP2.1. 

Recently, \citet{Levy:2014tp} showed that the objective function of the SGNS
algorithm is globally maximized when
\begin{equation}
  \vec{w}\cdot\vec{c} = PMI(w, c) - \log k = \log\frac{P(w,c)}{P(w)P(c)k}
  \label{eqn:levy}
\end{equation}
where PMI is the well known Pointwise Mutual Information function and $k$
is a constant. This theoretical result shows that the information mined by SGNS
is identical to traditional distributional models of language, and SGNS
implicitly acts as a low-dimensional matrix factorization, similar to 
Singular Value Decomposition  or Non-negative Matrix
Factorization. But at this point the \emph{explicit} matrix
factorization used by SGNS is not known.

One way of thinking about the Word2Vec global objective in
Equation~(\ref{eqn:w2vglobal}) and its decomposition in Equation
(\ref{eqn:levy}) is by viewing it as classic logistic regression. If we take context
representations $\vec{c}$ to be fixed, then learning optimal word
representations $\vec{w}$ can be viewed as the task of learning 
classifiers which separate contexts which appear more likely than chance ($PMI
> 0$) from those that appear less likely than chance ($PMI <
0$). Conversely, if we view word representations $\vec{w}$ as fixed,
the task of learning context representations $\vec{c}$ can again be
viewed as regression. This suggests that one possible way to train SGNS or SGNS-like
representations is by training classical logistic regression classifiers for
words, then training classifiers for contexts, and iterating until global
convergence. In this project, we will work on theoretically deriving and empirically verifying the Word2Vec matrix
factorization objective.

Expressing Word2Vec as an explicit matrix factorization would
allow it to be extended to factorize syntactic and bag-of-words
matrices simultaneously, resulting in representations which jointly model both
the narrow syntactic context and a wide bag-of-words context.
We plan to use the same two-part approach as
\citet{vandecruys-poibeau-korhonen:2011:EMNLP}, estimating a joint
representation by iteratively alternating between maximizing syntactic
representation and the wider bag-of-words representation. 

We think that this joint model of syntactic and bag-of-words representations
will allow for improvements on all of our proposed tasks. But we
hypothesize that 
it will be particularly useful for substitutes that are not in a
standard relation to the target, like ``explain'', ``instruct'', and
``teach'' in the example in Table~\ref{tab:lexsub_example}. (As
mentioned above, more than 60\% of the substitutes in the 
\citet{coinco} dataset were {\em not} synonyms, hyponyms, or
hypernyms of the target according to WordNet.) We suspect that
``core'' (that is, WordNet-related) substitutes 
may be more easily inferrable through syntactic context while the
``noncore'' substitutes will be more in need of information from the
wider context. We will test this hypothesis directly with the
\citet{coinco} dataset, where core and noncore substitutes are already
marked, and we will also test whether it is possible to rank
substitutes in a two-step process, first focusing on core substitutes,
and then in a second step ranking non-core substitutes using the core
substitute ranking as additional input.




% \begin{itemize}
%   \item Write up both w2v models
%   \item w2v has shown marked improvements on numerous tasks it's used for
%     \begin{itemize}
%       \item But deconstruction shows it's related, indicating as a factorization
%         it's benefiting, especialling over other factorizations like SVD
%     \end{itemize}
%   \item our goals are to:
%     \begin{itemize}
%       \item Show its natural application to word meaning in context works
%       \item Propose more sophisticated methods for inferencing vectors in context
%     \end{itemize}
%   \item Extending word meaning in context
%     \begin{itemize}
%       \item Previous models of employ only mostly syntactic spaces,
%         finding that BOW models perform poorly.
%       \item But this has to do with syntactic and selectional preference constraints
%         of those generated!
%       \item Can BOW models, which highlight general and topical relationships between
%         words, be used to extend the syntactic model?
%         \begin{itemize}
%           \item Approaches are similar to those of multimodal models of word meaning
%           \item With an explicit form of the factorization,
%             there is a clear way forward
%           \item Additionally, we hope it will improve converage of non-canonical
%             substitutes, and a thorough analysis by substitute tier and shit
%             is needed.
%         \end{itemize}
%       \item Many of the successful models create models of word meaning in context
%         via interpolating and smoothing using the nearest neighbors of the words
%         in context. Can this same approach be applied to non-explicit spaces like
%         w2v or glove?
%     \end{itemize}
% \end{itemize}
% 
% \paragraph{Lexical substitution for testing context-specific word
%   meaning representations.} Hypothesis that we
% follow: word meaning in context should be represented in graded and
% flexible fashion. Two representations in particular: representation as
% point in space~\citep{ErkPado:08}, weight
% assignments to substitutes~\citep{Moon:2013}. Like previous work on graded
% meaning
% representations~\citep{dinu-lapata:2010:EMNLP,ErkPado:10,ThaterFuerstenauPinkal:10,thater11:word_meanin_context,vandecruys-poibeau-korhonen:2011:EMNLP,Dinu2012},
% evaluation on lexical substitution task~\citep{MccarthyNavigli:09}.
% 
% State of the art in lexical substitution as unsupervised task is
% \citet{Dinu2012}. Core points that we want to improve: (i) They use
% syntactic neighbors only, ignoring most of the information in the
% sentence. BOW must be useful somehow. (ii) \citet{Moon:2013} noted the
% usefulness of observed co-occurrence, while current papers usually
% never compare the actual words, just their
% representations. Distributional smoothing probably useful, but
% observed co-occurrence needs to be taken into account too. 
% 
% \paragraph{Proposed models.} \textbf{Stephen, can you do a first draft
%   of this?}
% \begin{itemize}
% \item Model 1: simple conditional probability based model of word meaning in
%   context: focus on observed co-occurrence, according to first
%   experiments beats current state of the
%   art on SemEval2007 dataset
% \item combination of syntactic and BOW information: either within the
%   simple conditional probability model, or based on explicit
%   factorization and based on your prior word on multi-modal
%   distributional models
% \item exploring explicit cooccurrence and distributional smoothing:
%   how can they be combined?
% \item further thing to explore: regularities in co-occurrence of substitutes
% \item further thing to explore: distinguishing core and non-core
%   substitutes. Core: synonyms, hypernyms, hyponyms according to
%   WordNet, ConceptNet or what have you. Use those along with wider
%   context of the sentence to infer additional substitutes not related
%   in WordNet, such as ``explain'' and ``teach'' in the Julia Childs
%   sentence. 
% \end{itemize}

