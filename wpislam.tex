% Inference in a Markov Logic Network (MLN) differs from
% standard first-order inference in several respects. To do inference,
% Markov Logic Network compute all groundings of a given set of formulas $F$
% and construct a Markov network that represents this set of ground
% formulas. This makes Markov Logic Networks highly memory intensive as
% the number of constants and the size of $F$ grow. Another issue is
% that in
% order to be able to equate the probability of a possible world with
% the probability of a truth assignment to the set of ground formulas,
% MLNs have to make the \emph{domain closure assumption}, the assumption
% of a one-to-one correspondence between constants in $F$ and objects
% in the domain. Under the domain closure assumption, ``Tweety is a bird
% that flies'' entails that ``all birds fly'' because inference is done
% over a domain of size one. The weights on weighted formulas allow for
% inference over uncertain knowledge, such that it becomes possible to
% compute the \emph{probability} of a query formula. However, this
% probability is difficult to interpret, as it is dependent on the
% domain size. In a domain of size one, and with no additional evidence,
% the probability of $\exists x.p(x)$ is 0.5, while in a domain of size
% two the probability is 0.75. 

In this work package we focus on probabilistic inference over
logic-based representations of natural language sentences. The task
on which we evaluate is Textual Entailment (RTE), using the standard
RTE datasets as well as the FraCas dataset, a dataset tailored to
specific semantic phenomena~\citep{cooper1996using} and the SICK
dataset, tailored to exclude world knowledge~\citep{marelli:lrec14}.

\paragraph{WP1.1 The closed world assumption.} MLNs perform inference
on a network based on the set of all groundings of a given set of
formulas. This network can become very large, leading to scalability
issues that already manifest in RTE if the Text and Hypothesis in
question are reasonably long sentences. In prior work we have
addressed this issue through a
(modified) closed world assumption (CWA): Any ground atom that it
\emph{unreachable} from the evidence is assumed false~\citep{beltagy:starai14}. Adding
the corresponding negative evidence to the system leads to a
dramatically reduced network size.\footnote{Recent advances in lifted
  inference~\citep{singla:aaai08,gogate:uai11} are not applicable in
  our case. Lifted inference relies on finding structural regularities
  (symmetries) in the MLN
  and avoiding grounding repeated structures. However, for these
  regularities to be frequent enough, the implicit assumption is that
  formulae are typically short and the set of evidence is large. These
  assumptions do not fit the inference problems we are addressing,
  which involve a limited number of ground facts but complex logical
  formulas.} %Addition of the modified CWA
%also largely eliminates the domain size sensitivity of
%the computed probabilities. 
However, in its current form the CWA leads
to problems. For example ``Tweety is a bird'' implies the Query ``No bird
flies'' because there is no evidence that Tweety (the sole member of
the domain) flies. This leads to a general problem with negation in
the Query. We are currently successfully addressing this issue by generating soft
evidence contradicting the negative query, basically an anti-CWA (in the case above, ``there
is a bird, and with a certain weight it flies''), but we would like to
pursue a more principled approach. We would like to amend the CWA to distinguish betwen
\emph{impossible} propositions and \emph{possible but unsupported}
propositions. We want to apply the CWA (that is, generate negative
evidence) for impossible propositions but not possible but unsupported
ones, with the hope being that the network, while larger than with the
current CWA, will still be small enough to make the approach
feasible. In its most general form, of course, this
problem amounts to ``solving'' the problem of world knowledge, but we
think that it is possible to solve part of the problem with simple
means, using both hard semantic types and soft selectional
preferences. For example, assume we know of the constant $T$ that
$bird(T)$ holds. 
\begin{description}
\item[impossible:] $T$ cannot be a flying event as objects and events
  are disjoint. This should be
  excluded by semantic types.
\item[possible:] T can be the agent of a flying event. This should be
  supported by semantic types.
\item[unlikely:] $T$ is unlikely to be the agent of
  a voting event, as selectional preferences should indicate.
\end{description}
A taxonomy like WordNet can be used to enforce semantic types. An existing simple distributional approach to
selectional preferences that we developed can be used to
determine these soft preferences~\citep{epp-selpref}. Soft preferences
can be used both to reduce network size (by choosing a cutoff point
beyond which a proposition is considered so unlikely that it is
excluded) and to provide additional soft inference rules. 

Without any CWA,
the standard probability of a literal $\ell$ of which nothing is known
is $0.5$. With our current CWA, it is zero. With the distinction of possible versus impossible
propositions, it should again become possible to obtain a probability
of close to 0.5 for many propositions that are possible but not
supported by evidence. This would enable us to rephrase the inference
problem of whether Query $Q$ follows from evidence $E$ (omitting the
inference rules $F_w$ for simplicity) as computing
\begin{equation}
  \frac{P(Q|E)}{P(Q)}
  \label{eqn:pqepq}
\end{equation}
 instead of just
 $P(Q|E)$. Equation (\ref{eqn:pqepq}) is the ratio of the prior
probability of the Query with its posterior given the evidence. This ratio
should be high if the Evidence supports the Query, around 1 if Query
and Evidence are unrelated, and low if the Evidence contradicts the
Query, allowing for better RTE inference than our current
formulation. The new formulation cannot be used with the current CWA
because of its distorted (high) value of $P(Q)$ if $Q$ contains
negated literals. Without any CWA the formulation in terms of a
posterior/prior ratio is 
also not possible because of memory limitations, so we need the CWA
with possible/impossible distinction for this. 


\paragraph{WP1.2 Integrating weighted lexical information.} We will make use
of several types of lexical information: (a) distributional similarity
turned into distributional inference rules, (b) 
automatically constructed repositories of phrasal
paraphrases~\citep{lin:kdd01,Berant:2011te,Ganitkevitch:2013tf}, and
(c) manually constructed resources like
WordNet~\citep{Fellbaum:98}. Adding unweighted rules from type (c)
resources is straightforward, and we found, consistently with previous
research on logic-based approaches~\citep{bjerva-EtAl:2014:SemEval}, that inference rules from such resources improve
performance. With respect to (a), we have said above that the system turns a distributional similarity $w = sim(u_1, u_2)$ of two words or
phrases $u_1, u_2$ into an inference rule of the form $\forall
x. u_1(x) \to u_2(x) | f(w)$. But up to now, we
have been using distributional word representations that did not take
the sentence context into account. So for distributional inference
rules, the main aim of the current project is to move to context-aware
distributional representations as reported below under WP2. A
secondary aim that we pursue is to further explore phrase similarity
measures beyond the simple vector addition currently used in our system. We do not
aim to develop new phrase similarity measures, but our framework will make a
good testbed for testing existing compositional distributional approaches to
computing phrase
similarity~\citep{mitchell:acl08,mitchell:jcs10,baroni-zamparelli:2010:EMNLP,grefenstette:emnlp11,NIPS2011_0538,Socher:tm,Paperno:2014we}
on the textual entailment task. 

Concerning (b), all existing paraphrase
collections are formulated in terms of word sequences or syntactic
patterns, while logic-based approaches need inference rules in logical
form, so paraphrase collections need to be translated to logical
inference rules. For
sufficiently frequent and simple paraphrase patterns, we are defining
rule-based templates to perform the translation. For more
complex and rare paraphrase pairs, we will apply them to a raw
text sentence, translate the original as well as the paraphrase-transformed
sentence to logic, and use a variant of
Robinson resolution~\citep{Robinson} to extract the difference of the two
sentences as an inference rule in logic.

When multiple sources of weighted inference rules are used, one
potential problem is that the weights coming from different sources
may be at different scales. For example, most cosine similarity values
will be at around 0.2-0.5, but some paraphrase collections may make
use of the 0-1 range more equally. We will pursue a global weight learning
strategy based on \citet{zirn-EtAl:2011:IJCNLP-2011}, who learn a single
resource-specific scale for each resource. Translated to standard MLN
syntax\footnote{Mathias Niepert, p.c.}, the idea is as
follows: Suppose we obtain
weighted  information from resource $R$ about constants having
property $prop$. Then we introduce a predicate $prop_R$, ``an entity
has property $prop$ according to $R$'', and add evidence of the form
\[\begin{array}{l}
prop_R(C_1) \mid w_1\\
prop_R(C_2) \mid w_2\\
  \end{array}
  \]
where $w_1, w_2$ are the weights as $R$ states them. 
We additionally introduce a weighted rule that links $prop_R(x)$ to
$prop(x)$: If $R$ states that an entity $x$ has the
property $prop$, then that entity actually has property $prop$ with
some weight:
\[\forall x. prop_R(x) \leftrightarrow prop(x) \mid w_R\]
The global weight $w_R$ is learned based on the accuracy of the
predictions of $R$ on a training set. The amount of training data needed should
be relatively low, as we learn only one parameter per resource. 
 The approach will be adapted for the more complex
inferences coming from resources $R$ in our case.

\paragraph{WP1.3 Generalized quantifiers.}
The determiners ``all'' and ``some'' can be expressed directly in
first-order logic (FOL). Some others are FOL-expressible,
such as ``at most three''. But ``most'' and
``few'' cannot be expressed in FOL~\citep{BarwiseCooperGQ}. 
The standard analysis represents a determiner like ``most'' as a
function \textsc{most} that combines with two properties $A$ and $B$
such that \textsc{most}$(A)(B)$ is true iff $|A \cap B| >
\frac{1}{2}|A|$~\citep{Keenan:2002wp} -- which can only be stated in
higher-order logic. In probabilistic logic, we 
have an alternative to this cardinality-based characterization. If most A's are B's, then the probability that an
arbitrary A would also be a B is above
0.5. Using probabilities as weights for simplicity of exposition, we
could for example express ``most birds fly'' through a rule 
\[bird(x) \to fly(x) \mid 0.6.\]
This formalization allows for some inferences that the
cardinality-based characterization does not provide: If we know that
``most birds fly'' and Tweety is a bird, then we can conclude that
with high probability Tweety can fly. The determiner ``few'' can be
treated in a similar fashion. 
In the case of ``most birds fly'', this representation suffices,
but consider the sentence ``all birds eat most fruit''. We cannot
represent it as $bird(x) \wedge fruit(y) \to eat(x, y) \mid 0.6$
because that is instead the representation of ``mostly, birds eat
fruit'', which is not the same thing as it gives a nonzero probability
to a world where some bird would not eat any fruit at
all (but this shows that ``mostly'' can also receive a probabilistic
treatment). Instead we need to extend the representation with
auxiliary predicates. We obtain: $bird(x) \to eat\_most\_fruit(x)$ with infinite
weight, as well as $eat\_most\_fruit(x) \wedge fruit(y) \to eat(x, y) \mid
0.6$. A query of ``most birds fly'' would be done by querying ``any bird
flies''. If the probability of ``any bird flies'' is above 0.5, ``most
birds fly'' would be accepted. 

However, this framework breaks down when the CWA in its current form
(rather than the new form proposed here) is in place. With the
CWA, if ``most birds are small'' and ``most 
small things fly'', then it follows that ``few birds fly''  (that is,
a query of ``any bird flies'' would yield a low probability) because without any evidence on
non-small things flying, the probability that an entity would fly if
it is not small would be set to zero. So the problem is again that the
probability in the absence of information under the CWA is 0, not
0.5. (Another way of looking at this is that with the CWA, ``any bird
flies'' would get a low probability because we do not have information
about non-small things flying, while the original query of ``few birds
fly'' should be answered in the positive only if we have positive information that most birds do not
fly.) There are two options of addressing this problem. One is again
the use of an adapted CWA that tries to distinguish possible and impossible
propositions, as outlined in WP1.1. The second option is to introduce
explicit representations of ``most'' and ``few'', such that a query of ``few birds fly'' will only
succeed if there is explicit evidence that the maximal subset of birds
that flies contains few birds (rather than
insufficient evidence of birds flying, as in the framework above). In
that case, we need to axiomatize
the behavior of generalized quantifiers explicitly, based on the
approach of \citet{Schwertel:2005vs}. For example, to represent the
evidence that ``most birds fly'', we would introduce set-valued
constants $B_0$ for the set of all birds and $B_1$ for a set
containing most birds and all flying birds: 
\[
\begin{array}{l}
set(B_0)\\
set(B_1)\\
relation(B_1, most, B_0)\\
mem(x, B_0) \leftrightarrow bird(x)\\
mem(x, B_1) \to mem(x, B_0)\\
mem(x, B_1) \to fly(x)\\
bird(x) \to mem(x, B_1) \mid 0.6\\
bird(x) \wedge fly(x) \to mem(x, B_1)\\
\end{array}
\]
Note that the framework still involves a weighted rule, such that if
Tweety is a bird, there is a high probability that Tweety is a member
of $B_1$. In this setting, a query of ``few birds fly'' will succeed
only  if there is some constant $B_2$ that is a set whose relation to $B_0$ is ``few'' that
contains all flying
birds. 




Before we explore probabilistic encodings of generalized quantifiers,
we need to add representation of quantifiers to the wide-coverage semantic
analysis system we are using.

%%%%%%%%%%%%%%%
\paragraph{WP1.4 Inference inspection.} For the development of textual
entailment systems, it is extremely useful to be able to inspect the
inference process to determine which rules led to a given result. This
is for example available in the BIUTEE
system~\citep{stern:lilt13}. The construction of such a tool is not at
all straightforward for MLNs, because in Markov logic all rules
affect the outcome. However, some rules affect it
dramatically, others only slightly. To identify the
rules with the most impact, we will sample worlds and then check, for
each ground rule, how many samples satisfy it, taking sample probability
into account. As the inference
algorithm we use is already sample-based, we can simply use the
existing samples for inference
inspection. This sample-based technique will also let us group rules
by the samples in which they are satisfied, such that we can identify
rules that tend to co-apply in the same worlds. 
