The aim in this work package is to explore models of how
distributional information contributes to human language understanding
in a framework of formal semantics. \citet{ErkDraft} provides the general framework that we will use in this
part of the project. That is, we will further pursue the ideas that
(a) distributionally similar words share similar semantic features\footnote{At least
when similarity is based on syntactic or narrow-window distributional
spaces. Spaces with wide windows seem to behave
differently~\citep{Peirsman,Baroni:BLESS}.}, such that properties can
be inferred from distributional information, and (b) the interaction
between grounded, truth-conditional lexical knowledge and
distributional information can be described in terms of probabilistic
logic. 


\paragraph{WP3.1 Distributional models and semantic features.} Can the
dimensions of distributional models be viewed as semantic features or
primitives~\citep{schank-tesler69,wilks75,winograd78,fodor+80,jackendoff90,geeraerts10}? While there is no single universally acknowledged
characterization of semantic features, they are sometimes
characterized as being primitive or irreducible, which distributional
dimensions are not: In many cases, they can be given distributional
characterizations of their own. Also, individual semantic features are
used in inferences, while distributional dimensions usually do not
lend themselves to inferences individually, only in the
aggregate~\citep{Lenci:08}. So we will argue that distributional
dimensions are not the same as semantic features. But we will
hypothesize that both explicit semantic features and distributional
patterns are used in the lexicon, with the following argument. Some
semantic features are clearly prominent, and also relevant for
typology. But it is notoriously difficult to describe all of a word's
meaning in terms of explicitly defined semantic features -- so that
part of the meaning may be better captured in terms of patterns over
dimensions that are individually anonymous but meaningful in the
aggregate. However, if part of the meaning is specified in terms of
distributional patterns, how do they specify truth
conditions if they only describe words through other words? We will argue that distributional patterns
probabilistically map to \textit{implicit semantic features} (which
are characterizable in terms of truth conditions, but may be hard to
specify in words). 
While our previous work argued that semantic features can be inferred from
distributional patterns~\citep{ErkDraft}, it does not provide a
mechanism for inferring individual semantic features. But there are
existing computational mechanisms that can be used for this, for
instance in multimodal distributional
approaches that infer perceptual vectors from textual vectors and vice
versa~\citep{AndrewsViglioccoVinson:09,Johns:2012gn,Silberer:2012ug,Roller:13,Lazaridou:14}. We
will test these approaches in practice as computational models for inferring
semantic features from distributional patterns, and will develop a
theoretical framework on this basis.
 


% What is crucial to this account is to work out what distributional
% inference is, in particular, how distributional representations link
% to representations that are grounded in the world, like semantic
% features are~\citep{Zeevat:2014wt}. For this, we will build on
% \citet{ErkDraft}, where we argue that distributional inference is
% property inference, that is, that distributionally similar words have
% similar properties. So if it is possible to infer
% properties from distributional representation, this provides the link
% between distributional evidence and knowledge about the world. 
% The main aim of this work package is to work out the comparison of
% distributional dimensions and semantic features, to demonstrate how
% distributional patterns can be used to infer semantic features or
% properties, and to work out a framework that describes the interaction
% between the two levels of representation. 



% While semantic primitives have been criticized both on philosophical
% \cite{fodor+80} and psychological grounds \cite{fodor+75}, they are
% important typologically, and they provide a straightforward account
% for how a lexical representation would support
% inferences~\citep{Zeevat:2014wt}. In this project, we
% will need to work out (a) the differences between distributional and
% primitive-based approaches, and (b) how distributional approaches
% support inference, one of the core purposes of primitive-based
% approaches. Concerning (a), distributional features are 



% We argue that these inferences are \emph{property
%   inferences},. Additional evidence for this comes from a study of 
% \citet{Baroni:BLESS}, who find that co-hyponymy pairs achieve
% particularly high distributional similarity ratings. Co-hyponymy is
% not a well-defined relation as it is resource-dependent, but
% co-hyponyms are words that have many properties in common, so what
% Baroni and Lenci call co-hyponymy may actually be property overlap. 


\paragraph{WP3.2 Describing word meaning in context.} % \citet{Asher} describes meaning in context as
% determined by selectional constraints, which introduce type
% presuppositions from a complex system of types. 
\citet{Zeevat:2014wt} characterize the meaning of a word
through a collection of semantic features covering all senses of the word
(\emph{overspecification}~\citep{Smolensky:91,Hogeweg}), 
from which the maximal set of fitting features is then chosen for a given
context based on information from syntactic neighbors. This account is
remarkably similar
to distributional approaches to word meaning in context that 
compute a single vector for a word based on all its observed contexts
(similar to the overspecification in Zeevat et al.) and then re-weight the dimensions to better fit a given sentence
context, focussing on syntactic neighborhood as a source of
evidence~\citep{ErkPado:08,ThaterFuerstenauPinkal:10,thater11:word_meanin_context,vandecruys-poibeau-korhonen:2011:EMNLP}. In
this work package, we will pursue this connection by developing an account of word meaning in context
that relies on \emph{both} semantic features and distributional
information, building on the approach of Zeevat et al.\ and on the
link between distributional patterns and semantic features from
WP3.1. While WP3.1  only considers the link between
semantic features and distributional representation for a single word,
the question now is how the context-specific representation of a word
depends on the semantic features and distributional information of
adjacent words and their selectional constraints. We will construct a
formal model building on existing computational models of word meaning in
context. 

We will also compare to \citet{Asher}, who, like Zeevat et al., describes meaning in context as
determined by selectional constraints. Interestingly these selectional
constraints are stated through types which are conceptual,
mind-dependent entities, linked to intensions and extensions through a
``tracking'' mechanism that is not explicated further. So it will be
interesting to compare Asher's types to distributional
representations, as in psychology distributional information has been discussed as potential part of human conceptual
representation~\citep{LandauerDumais:97,McDonaldRamscar:01,Louwerse:07,AndrewsViglioccoVinson:09,Johns:2012gn}.


% A point that needs to be worked out is that while semantic
% features have a notion of consistency based on truth conditions,
% distributional models have a notion of ``consistency'' of their own
% based on observed co-occurrence; it can also be used to determine word
% meaning in context, but it is not the same notion of
% consistency. 
Another issue to be considered is the influence of wider discourse
context, not just direct syntactic neighborhood, on word
meaning. That this influence exists is clear -- examples are given
e.g.\ in \citet{Hanks:00}. But it is difficult to describe in a compositional
semantics setting, which allows for interaction only between syntactic
neighbors. We will explore whether the influence of wider
context on word meaning can be described using dynamic
semantics~\citep{kamp:book93,Veltman:1996wb}, because it explicitly
represents the context of an utterance and describes meaning as the
potential to change this context. 


% A central question will be how
% distributional patterns can be used to take into account not only hard
% constraints on the combination of predicates with arguments, but also
% preferences and tendencies. An important source to compare to will be
% the book on word meaning in context by \citet{Asher}. In this book,


% The area where we
% think this role is clearest is for determining the meaning of a word
% in context. There are distributional models that compute
% representations for word instances and use them predict contextually
% appropriate
% substitutes~\citep{ErkPado:08,dinu-lapata:2010:EMNLP,ErkPado:10,ThaterFuerstenauPinkal:10,thater11:word_meanin_context,vandecruys-poibeau-korhonen:2011:EMNLP,Dinu2012},
% and we aim to develop additional such models in WP2. 
