\section{Introduction}


Tasks in natural language semantics, like textual entailment or opinion analysis, are requiring increasingly complex and
fine-grained inference, including the use of negation~\citep{choi-cardie:2008:EMNLP} or embedded clauses~\citep{MacCartney:2009wr,lotan-stern-dagan:2013:NAACL-HLT}. What is the best representation format on which to perform these 
inferences? One hypothesis that is being explored is that a
syntactic representation will be best
suited~\citep{BarHaimEtAl:07,MacCartney:2009wr}, but that makes it necessary to apply heuristics for example to identify the scope of negation and the body and restriction of quantifiers within a dependency structure, issues that disappear when inference is done directly on logical form. The hypothesis
that we will pursue in this project is that \textit{a logical form representation is
best suited for supporting such inferences}. Logical approaches to
semantics have a long
tradition~\citep{montague:theoria70,dowty:book81,kamp:book93}, and
have principled ways to handle phenomena like negation and embedded
clauses, but they need to be combined with broad-coverage lexical
resources to be successful in tasks like textual entailment
\citep{bos:lilt13}. 
 We hypothesize that \emph{another crucial aspect will be the ability to reason with uncertain, probabilistic
information at the lexical level}. (The opposite hypothesis, that weighted lexical information needs to be made binary for inference at the sentence level, is explored for example by \citet{bjerva-EtAl:2014:SemEval} and \citet{Lewis:tf}.) 
Lexical meaning is
notoriously fluid and hard to characterize in terms of single
senses~\citep{passonneau:etal:2010,Erk:2010}, so we explore graded and
flexible distributional representations of word meanings that can be
used to predict
substitutes~\citep{ErkPado:08,dinu-lapata:2010:EMNLP,vandecruys-poibeau-korhonen:2011:EMNLP,thater11:word_meanin_context,Dinu2012}. Our
framework is flexible enough to also let us use existing manually and automatically compiled
lexical resources. In particular automatically compiled resources 
typically come with a weight that specifies
degree of certainty~\citep{lin:kdd01,Ganitkevitch:2013tf}, information which can only be used in a weighted inference setting. 



% Logical approaches to semantics have a long tradition~\citep{montague:theoria70,dowty:book81,kamp:book93}. They handle many complex phenomena such as embedded propositions, negation, and quantifiers. With more complex tasks in natural language semantics, such as Textual Entailment ~\citep{dagan:hlt13}, Semantic Parsing~\citep{kwiatkowski:emnlp13,berant:emnlp13}, or fine-grained opinion analysis, there has been a renewed interest in phenomena like negation and embedded propositions~\citep{choi-cardie:2008:EMNLP,MacCartney:2009wr,lotan-stern-dagan:2013:NAACL-HLT}. 
% What will be the best representation format for inferences involving such phenomena? One hypothesis that is being explored is that a
% syntactic representation will be best
% suited~\citep{BarHaimEtAl:07,MacCartney:2009wr}. But the hypothesis
% that we will pursue in this project is that \textit{a logical form representation is
% best suited for supporting such inferences}.
% \citet{bos:lilt13} identifies the
% use of broad-coverage lexical resources as one aspect that is crucial
% to the success of logic-based approaches. We hypothesize that \emph{another crucial aspect will be the ability to reason with uncertain, probabilistic
% information at the lexical level}. ~\citep{garrette:iwcs11,beltagy:starsem13}. Lexical information typically comes with weights, for example
% weights of paraphrase rules~\citet{lin:kdd01,Ganitkevitch:2013tf} or confidence ratings of word sense disambiguation systems. 
% Distributional models~\citep{TurneyPantel:10,LandauerDumais:97,ErkPado:08,mikolov:nips13} constitute a particularly interesting source of uncertain lexical information. 
% Based on the observation that semantically similar
% words and phrases occur in similar contexts, they use observed co-occurrences in large text corpora to compute representations that allow for inferences based on semantic similarity. To reason with such graded lexical information in a principled manner and to find an overall best interpretation requires the ability to handle weights. 

We have been developing an approach to semantics that uses probabilistic logic~\citep{Nilsson:1986te} to integrate logical form representations of sentence meaning with graded representations of lexical meaning~\citep{garrette:iwcs11,Garrette:bkchapter13,beltagy:starsem13,beltagy:acl14,beltagy:sp14,beltagy:starai14}.
For the probabilistic inference we build on Statistical Relational Learning/AI~\citep{getoor:book07}, which develops methods 
that combine logical and probabilistic
approaches. We use a
very expressive formalism called Markov Logic Networks 
(MLNs) \citep{richardson:mlj06, domingos:book09} to combine detailed
logical forms produced by a Combinatory Categorial Grammar (CCG) parser
\citep{steedman:book00,clark:acl04,bos:iwcs05} with probabilistic knowledge
from paraphrase repositories as well as distributional similarity ratings. 

In the proposed project, we will build on our previous groundwork (1) to construct a probabilistic semantics framework that is more principled, more expressive and more robust; (2) to integrate it with a new distributional approach to determining word meaning in context for improved lexical inferences; and (3) to explore how the probabilistic semantics framework that we are developing helps in understanding the role that distributional information in the human lexicon. Concerning (1), the development of an improved framework for probabilistic semantics, probabilistic inference in practice typically requires the computation of the set of all groundings of a given set of formulas, which can be huge. Cutting down on the size of this set of groundings is key to keeping inference on natural language sentence semantics feasible~\citep{beltagy:starai14}. The approach that we currently follow assumes that everything is false unless stated otherwise. While this closed world assumption is standard in theorem proving, it is problematic in a textual entailment setting because a negative query like ``no birds fly'' will become true no matter what the given evidence is. We will study inexpensive methods for making our assumptions less strong (that is, assuming fewer propositions to be false) while keeping the size of the set of groundings manageable by using both hard typing and soft selectional constraints to decide on the negative assumptions to keep.
Second, the probabilistic logic setting offers the opportunity to represent semantic phenomena in probabilistic ways. If ``most birds fly'', then that means that the probability that any given bird will fly is above 0.5. This is particularly exciting as determiners like ``most'' and ``few'' are not expressible in first-order logic but could possibly be expressed in probabilistic first-order logic. Another tricky problem we will study is inference inspection: How can we determine how strongly different weighted rules affected a probability computed by an MLN? In systems that apply rules sequentially, this sequence can be expected. But in MLNs there is no such sequence, and all rules contribute to the computed probability of a query. So instead we will have to determine which rules affect the outcome most strongly. 

Concerning (2), the derivation of appropriate graded inferences at the lexical level, we will develop a new approach to representing word meaning in context through distributional models, as they can represent context-specific meaning in a flexible fashion without reference to dictionary senses. There are a number of existing models for this problem~\citep{ErkPado:08,dinu-lapata:2010:EMNLP,ErkPado:10,ReisingerMooney:10,ThaterFuerstenauPinkal:10,thater11:word_meanin_context,vandecruys-poibeau-korhonen:2011:EMNLP,Dinu2012}, but a number of important questions have not been sufficiently explored yet, 
notably the nature of the evidence used. Current state-of-the-art models rely exclusively on syntactic information to compute a context-specific representation~\citep{thater11:word_meanin_context,Dinu2012} while ignoring wider sentence context, which should intuitively be very useful given the features typically used in word sense disambiguation~\citep{LNC3:LNC3131}. We will explore a combination of both types of evidence. We build on Word2Vec~\citep{mikolov-yih-zweig:2013:NAACL-HLT}, a recent distributional approach that has proven very successful across many tasks~\citep{baroni-dinu-kruszewski:2014:P14-1}, and its analysis as matrix factorization~\citep{Levy:2014tp}. 


Concerning (3), the connection of logic and distributional information through probabilistic inference is interesting not only in a practical context but also in the context of formal semantics~\citep{Copestake:2012wm,Erk:IWCS13,Larsson:13,vanEijckLappin,Zeevat:2014wt}: Do people use distributional information in language understanding? And if so, how can an account of distributional inference be integrated with formal semantics? We have recently proposed an account of how distributional information can influence assumed word meaning, an account that crucially relies on the idea that words that are distributionally similar tend to have similar semantic features or properties~\citep{ErkDraft} and that it is therefore possible to infer semantic features of a concept from its textual similarity to other concepts. We have used probabilistic logic to describe how a speaker can use distributional inference to update their information state. That paper, which focuses on inferring part of the meaning of unknown words, only constitutes a first step; in the current project, we will focus on the question of how distributional information, through its links to properties, can contribute to characterizing word meaning in context. 

This project addresses three areas, each of which are difficult and relatively recent: probabilistic inference for logic-based
semantics, graded and flexible computational representations for word meaning in
context, and in the question of where distributional information fits
in with formal semantics. We believe that it is possible to make progress on all three areas within this project, based on
our prior work on them, as we have a first prototype system that does probabilistic inference over logical form representations of sentence meaning~\citep{garrette:iwcs11,Garrette:bkchapter13,beltagy:starsem13,beltagy:acl14,beltagy:sp14,beltagy:starai14}, we have previously proposed both models and datasets for graded representations in context~\citep{ErkPado:08,ErkPado:10,ErkMCGaylord:12,Moon:2013,coinco}, and we have done some first steps on the use of distributional information in formal semantics~\citep{Erk:IWCS13,ErkDraft}. 

We believe that this is a good time to take on this
project. The basis on which we build is getting stronger, as more wide-coverage systems for logical form analysis are becoming
available~\citep{CopestakeFlickinger,bos:step08,Lewis:tf}, and
progress is being made in effective inference with Markov
Logic Networks even for larger problem sizes (e.g.,
\citep{gogate:aistats07}). And while there are not many approaches that make inferences based on wide-coverage logical form analyses like we do, there are some~\citep{Lewis:tf,bjerva-EtAl:2014:SemEval}. The area of our second focus, distributional semantics, is currently seeing 
a new
boom because of deep learning models (e.g.,
\citep{Collobert:2008vb,mikolov-yih-zweig:2013:NAACL-HLT,Mikolov:2013tp,Levy:2014tp,baroni-dinu-kruszewski:2014:P14-1,fu-EtAl:2014:P14-1,hermann-blunsom:2014:P14-1}), which fits in well with our aim of adapting Word2Vec for the task of characterizing word meaning in context. And
in
formal semantics, there is a beginning discussion of the role of
distributional information~\citep{Copestake:2012wm,vanEijckLappin,Erk:IWCS13,Larsson:13,ErkDraft}.  



% Graded representation of word meaning: 
% Word senses are often hard to pin down, and the task of word sense
% disambiguation is still a challenge. This could in fact be due to a fundamental problem, as work in both cognitive linguistics and lexicography has drawn the assumption of senses with clear boundaries into doubt~\citep{Tuggy93,Cruse:95,Kilgarriff:97,Hanks:00}. Instead we explore graded, flexible mechanisms for representing word meaning in context, including distributional information~\citep{ErkPado:08,dinu-lapata:2010:EMNLP,ErkPado:10,ThaterFuerstenauPinkal:10,thater11:word_meanin_context,vandecruys-poibeau-korhonen:2011:EMNLP,Dinu2012}, substitute distributions~\citep{DeschachtMoens:09,Moon:2013}, and graded sense assignment~\citep{ErkMcCarthy:09}. At the technical level, build on \citet{mikolov-yih-zweig:2013:NAACL-HLT}, recent approach to computing word embeddings, basis in deep learning, but with interpretation as implicit factorization of co-occurrence matrix~\citep{Levy:2014tp}. Easy to compute, word representations with very good results on many tasks~\citep{baroni-dinu-kruszewski:2014:P14-1}. Question that we ask: also useful for word meaning in context? \textbf{shorten this here, boil down to main point}

% While theorem proving for standard first-order logic is a mature area, probabilistic inference is a much younger field, and is 

% Markov Logic Networks are being widely used, but mostly not for reasoning over logical form representations of natural language sentences. 

% Markov Logic Networks perform probabilistic inference by constructing a Markov network out of all groundings if a given set of formulas. This network can become very large; restricting the size of the network based on the given set of formulas and based on world kwoeldge

% Probabilistic inference: enabling correct inferences while keeping complexity of the inference problem manageable; inspecting inference process; making use of the probabilistic nature of the system in the representation of language phenomena, in particular quantifiers; inclusion of graded information from multiple sources. 

% Graded representation of word meaning: 
% Word senses are often hard to pin down, and the task of word sense
% disambiguation is still a challenge. This could in fact be due to a fundamental problem, as work in both cognitive linguistics and lexicography has drawn the assumption of senses with clear boundaries into doubt~\citep{Tuggy93,Cruse:95,Kilgarriff:97,Hanks:00}. Instead we explore graded, flexible mechanisms for representing word meaning in context, including distributional information~\citep{ErkPado:08,dinu-lapata:2010:EMNLP,ErkPado:10,ThaterFuerstenauPinkal:10,thater11:word_meanin_context,vandecruys-poibeau-korhonen:2011:EMNLP,Dinu2012}, substitute distributions~\citep{DeschachtMoens:09,Moon:2013}, and graded sense assignment~\citep{ErkMcCarthy:09}. At the technical level, build on \citet{mikolov-yih-zweig:2013:NAACL-HLT}, recent approach to computing word embeddings, basis in deep learning, but with interpretation as implicit factorization of co-occurrence matrix~\citep{Levy:2014tp}. Easy to compute, word representations with very good results on many tasks~\citep{baroni-dinu-kruszewski:2014:P14-1}. Question that we ask: also useful for word meaning in context? \textbf{shorten this here, boil down to main point}

% Question of to what extent people use distributional information in language understanding often discussed~\citep{LandauerDumais:97,McDonaldRamscar:01,Murphy:02,Louwerse:07,Lenci:08,Barsalou:2008,AndrewsViglioccoVinson:09,Louwerse:2010}. More recently also the question of integration with a formal account of semantics~\citep{vanEijckLappin,Copestake:2012wm,,Larsson:13}. We recently started on account that assumes: 
% representation of lexical items both at a truth-conditional level, including explicit knowledge about relations between words, and at distributional level, encoding degrees of similarity. Explicit truth-conditional representation helps interpret distributional representations, and distributional representations reduce speaker's uncertainty about meaning of lexical items about whose meaning they are uncertain. Framed in probabilistic logic setting, with distributional evidence updating the probability distribution over worlds that represents the speaker's information status~\citep{ErkDraft}. Only starting point, in particular need to determine character of distributional inference, and role of distributional evidence in characterizing word meaning in context. 
% \textbf{same text in related work. shorten here.}



% In this project, study some fundamental questions on logic-based sentence semantics and distributional lexical semantics: 
% \begin{itemize}
% \item How best to represent sentence meaning for doing inference, and correctly handling phenomena like negation, factivity of embedded sentences, quantifier monotonicity: can logic-based compete with dependencies, AMR? 
% \item Principled way of integrating uncertain knowledge into logic-based semantics?
% \item Using probabilistic formulations to represent semantic phenomena, beyond word meaning of content words?
% \item Doing probabilistic inference at scale?
% \item At lexical semantic level, distributional models as a viable method for dealing with the flexible way in which context can affect word meaning? 
% \item What kinds of inferences do distributional models support? 
% \end{itemize}

% Why this is a good time to study questions like this: Questions of ``deeper'' sentence semantics coming up in current tasks, most obviously in textual entailment (negation and factivity, see Natural Logic papers, see Dagan group papers such as their TruthTeller paper), but also in sentiment analysis. Exploration of deeper meaning representations and inference other than logic: natural logic, AMR. Semantic parsing currently immensely popular, see recent workshop. Development of wide-coverage logical form parsers: Boxer, Lewis and Steedman, Delphin/MRS. Active development of probabilistic inference systems such as Markov Logic Networks, Probabilistic Soft Logic. 

% What we have: a system that uses Boxer to produce logical form representations of sentence meaning, MLNs for probabilistic inference, multiple sources of uncertain knowledge: cosine similarity of distributional word and phrase representations, paraphrases from Berant. Proof of concept system, discussed in the Montague Meets Markov paper. Since then: improvements in scalability for MLN inference, experiments with PSL. 

% What needs to be done next: On the logic-based semantics side:
% \begin{itemize}
% \item MLNs make domain closure assumption, only consider finite domains. This leads to wrong inferences if standard encoding of natural language semantics is used. Needed: develop alternative formulations, especially for universal quantifiers, that work with finite domains.
% \item Scaling up: remove impossible groundings to keep networks small. Current method not general, misses some cases. Make more general.
% \item Integrating uncertain knowledge from different sources: currently, all simply thrown into the pot. But different sources will have weights on different scales. Weight learning, one parameter per source.
% \item Some natural language phenomena can be phrased easily in weighted or probabilistic terms. First and foremost, quantifiers: ``most birds fly'' as ``when you encounter a bird, there is a good chance that it can fly''. Allows for inferences akin to those in default logics: ``Most birds fly. Tweety is a bird'' entails ``probably, Tweety can fly''. Modifiers like ``probably'', ``possibly'' another case where probabilistic formulation could be helpful. 
% \end{itemize}



% On the side of integrating distributional and formal semantics at a theoretical level:
% \begin{itemize}
% \item integration of uncertain distributional information with certain, symbolic knowledge
% \item distributional inference can help when we lack certain knowledge. Distributional information as soft meaning postulates.
% \item comparison of distributional models with its latent semantic classes (not very interpretable dimensions) with semantic primitives: inference from the overall pattern, the aggregate of all dimensions, rather than inference from the presence of a single primitive
% \item How do selectional constraints influence word meaning in context? In distributional approaches to lexical substitution, selectional preferences very important (Erk and Pado, Thater et al). On theory side, Asher's book encodes selectional information in a rich type system to determine word meaning in context. Build on both to develop theoretical model of how word meaning in context is determined
% \item theoretical considerations of this kind can help lay the foundation for practical approaches, so both are needed. 
% \end{itemize}


