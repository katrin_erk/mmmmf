% abstract, one page all in all
\section*{Project Summary}

Tasks in natural language semantics are requiring increasingly complex and
fine-grained inference, including the use of negation~\citep{choi-cardie:2008:EMNLP} or embedded clauses
~\citep{MacCartney:2009wr,lotan-stern-dagan:2013:NAACL-HLT}. What is
going to be the best representation format on which to perform these
inferences? One hypothesis that is being explored is that a
syntactic representation will be best
suited~\citep{BarHaimEtAl:07,MacCartney:2009wr}. The hypothesis
that we will pursue in this project is that \textit{a logical form representation is
best suited for supporting such inferences}, as there is a long
tradition of building and reasoning with logical approaches to
semantics~\citep{montague:theoria70,dowty:book81,kamp:book93}, and as
they 
have principled ways to handle phenomena like negation and embedded
clauses. However, they need to be combined with broad-coverage lexical
resources to be successful in tasks like textual entailment
\citep{bos:lilt13}. We hypothesize that in addition, \textit{it is
necessary to be able to reason explicitly about uncertain,
probabilistic information at the lexical level.} Lexical meaning is
notoriously fluid and hard to characterize in terms of single
senses~\citep{passonneau:etal:2010,Erk:2010}, so we explore graded and
flexible distributional representations of word meanings that can be
used to predict
substitutes~\citep{ErkPado:08,dinu-lapata:2010:EMNLP,vandecruys-poibeau-korhonen:2011:EMNLP,thater11:word_meanin_context,Dinu2012}. Our
framework is flexible enough to also let us use existing manually and automatically compiled
lexical resources. In particular automatically compiled resources 
typically come with a weight that specifies
certainty~\citep{lin:kdd01,Ganitkevitch:2013tf}, which is another
reason in favor of reasoning with uncertain, weighted  information. 

We build on a previous prototype system~\citep{garrette:iwcs11,Garrette:bkchapter13,beltagy:starsem13,beltagy:acl14,beltagy:sp14,beltagy:starai14}, which
represents sentence meaning in logical form, adds distributional
information through weighted distributional inference rules, and
performs probabilistic inference with Markov Logic
Networks~\citep{richardson:mlj06}. In the current project, we explore
principled solutions to problems of probabilistic inference and the distributional characterization of word meaning in
context. We explore the connection of logic and distributional information
through probabilistic inference in a practical
context, but also in the context of formal semantics.

\textbf{Intellectual merit.} 
This project will make progress in three
difficult areas: in probabilistic inference for logic-based
semantics, in graded and flexible representations for word meaning in
context, and in the question of where distributional information fits
in with formal semantics. We believe that this is possible based on
our prior work in all three areas~\citep{ErkPado:08,ErkPado:10,garrette:iwcs11,Erk:LLC,Moon:2013,Garrette:bkchapter13,beltagy:starsem13,Erk:IWCS13,beltagy:acl14,beltagy:sp14,beltagy:starai14,coinco,ErkDraft}. 
Among the concrete steps we propose are a better estimate of prior
knowledge, a new probabilistic
formalization of generalized quantifiers, a weighting scheme for
integration of multiple lexical resources in probabilistic inference,
and a new distributional model of word meaning in context that builds on the recent and highly successful
Word2Vec model~\citep{mikolov-yih-zweig:2013:NAACL-HLT}. 

This is a good time to take on this
project, as wide-coverage logical form systems are becoming
available~\citep{CopestakeFlickinger,bos:step08,Lewis:tf}, and
progress is being made in effective inference with Markov
Logic Networks even for larger problem sizes (e.g.,
\citep{gogate:aistats07}). Distributional semantics is seeing a new
boom because of deep learning models (e.g.,
\citep{Collobert:2008vb,mikolov-yih-zweig:2013:NAACL-HLT,Mikolov:2013tp,Levy:2014tp,baroni-dinu-kruszewski:2014:P14-1,fu-EtAl:2014:P14-1,hermann-blunsom:2014:P14-1}). And
in
formal semantics, there is a beginning discussion of the role of
distributional information~\citep{Copestake:2012wm,vanEijckLappin,Erk:IWCS13,Larsson:13,ErkDraft}.  



\textbf{Broader impact.}  
Both distributional and logic-based approaches are ideal for teaching: 
Distributional models are relatively easy to compute and highly
malleable through their parameters, while logic-based approaches will
be appealing to students who like to work in a rule-based framework. We will put an emphasis on student and researcher training,
through (graduate and undergraduate) research assistantships and class
projects. We are developing an undergraduate computational semantics
class that will offer small independent research projects. Also,
students are being encouraged to join an international
teleconference-based 
reading group on the foundations of distributional models. 
